#!/bin/bash

# Cd into the location of the shell script
cd "$(dirname "$0")/build"

pwd

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && make

cp ./compile_commands.json ../
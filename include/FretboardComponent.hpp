#pragma once

#include "tml.hpp"
#include <juce_gui_extra/juce_gui_extra.h>
#include <vector>

class FretboardComponent : public juce::Component {
 public:
   //==============================================================================
   FretboardComponent();

   //==============================================================================
   void paint(juce::Graphics &) override;
   void resized() override;

   // Mutators of the fretboard scale

   void RandomizeScaleAndRoot();
   void RandomizeScale();
   void RandomizeRoot();

 private:
   /// Stores the fretboard background image
   juce::Image fretboard_img_;

   /// Stores the circle asset image that we use to render for each circle
   juce::Image circle_img_;

   /// Stores the location info for the fretboard
   juce::Rectangle<int> fretboard_loc_;

   /// The first vec is the strings and the second is the frets
   std::vector<std::vector<juce::Rectangle<int>>> fret_circle_locs_;

   /// The Fretboard from tml
   tml::GuitarFretboard fretboard_;

   /// Information about the scale currently in the fretboard
   std::string scale_name_;

   /// Root currently in the fretboard
   tml::PitchName root_;

   /// Scale currently in the fretboard.
   tml::Scale scale_;

   /// Label of what is currently stored in the guitar and rendered to the screen
   juce::Label scale_label_;

   /// Randomizer button
   juce::TextButton randomizer_;

   JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FretboardComponent)
};

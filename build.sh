#!/bin/bash

# Cd into the location of the shell script
cd "$(dirname "$0")/build"

pwd

cmake -DCMAKE_C_COMPILER="/usr/bin/clang" -DCMAKE_CXX_COMPILER="/usr/bin/clang++" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. && make

cp ./compile_commands.json ../

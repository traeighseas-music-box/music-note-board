# Music Note Board (WIP)

A music theory plugin built with JUCE to store notes about your music within your DAW. Hopefully this can help bridge the gap between written sheet music and DAW productivity. Has the ability to view music theory across a guitar fretboard to store your music ideas. There will be both a cli (primarily for generative content) and a DAW plugin.

Note: My focus is moving tml to a more finished state but this project is being used as a user test / example of a project that will use the library.

## Features

- [ ] Guitar fretboard view
- [ ] DAW Plugin
- [ ] View MIDI notes on a music clef
- [ ] Export to PNG
- [ ] Store music theory information such as scale used in a section
- [ ] Store music theory information such as a motif used

## Requirements

To compile from source this will rely on tml [(Traeighsea's Music Theory Library)](https://gitlab.com/traeighsea/tml) for music theory information as well as JUCE for UI. Additionally at minimum C++14 is required. Cmake and Clang strongly suggested as per the build script.

## Prototype

You can find a photoshop file as well as a png of an example of what the output _might_ look like in the docs folder. Also included here:

![C Major Scale Guitar Fretboard Output](/docs/guitar_neck_C_major.png)


#include "MainComponent.hpp"

//==============================================================================
MainComponent::MainComponent() : fretboard_{} {
   setSize(1200, 700);
}

//==============================================================================
void MainComponent::paint(juce::Graphics &g) {
   fretboard_.paint(g);
}

void MainComponent::resized() {
   fretboard_.resized();
   // This is called when the MainComponent is resized.
   // If you add any child components, this is where you should
   // update their positions.
}

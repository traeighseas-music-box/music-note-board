#include "FretboardComponent.hpp"
#include "BinaryData.h"

namespace {
const int frets = 25;
}

//==============================================================================
FretboardComponent::FretboardComponent()
      : fretboard_img_{}, circle_img_{}, fretboard_loc_{}, fret_circle_locs_{}, fretboard_{},
        scale_name_{}, root_{}, scale_{}, scale_label_{}, randomizer_{} {
   setSize(1200, 700);

   juce::File file = juce::File();

   circle_img_ =
         juce::ImageFileFormat::loadFrom(BinaryData::circle_png, BinaryData::circle_pngSize);
   fretboard_img_ =
         juce::ImageFileFormat::loadFrom(BinaryData::fretboard_png, BinaryData::fretboard_pngSize);

   fretboard_loc_ = {100, 37, fretboard_img_.getWidth(), fretboard_img_.getHeight()};

   fret_circle_locs_.resize(6);
   // Special case for the 0th fret
   for (unsigned string = 0; string < 6; string++) {
      fret_circle_locs_[string].push_back(
            {0, (static_cast<int>(string) * 97), circle_img_.getWidth(), circle_img_.getHeight()});
   }
   for (unsigned string = 0; string < 6; string++) {
      for (int fret = 1; fret < 25; fret++) {
         fret_circle_locs_[string].push_back({(fret * 124 + 13), (static_cast<int>(string) * 97),
                                              circle_img_.getWidth(), circle_img_.getHeight()});
      }
   }
   srand(std::time(0));

   // Disable the unicode characters for now
   tml::USE_UNICODE_ACCIDENTALS = false;

   std::vector<std::string> scale_list = tml::ScaleDictionary::Instance().GetRegisteredNames();
   scale_name_ = scale_list[rand() % scale_list.size()];
   scale_ = {tml::ScaleDictionary::Instance().GetScale(root_, scale_name_)};
   fretboard_.SetFromScale(scale_);
   scale_label_.setText(scale_.GetName(), juce::dontSendNotification);
}

//==============================================================================
void FretboardComponent::paint(juce::Graphics &g) {
   // (Our component is opaque, so we must completely fill the background with a solid colour)
   g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));

   g.setFont(juce::Font(40.0f, juce::Font::bold));
   g.setColour(juce::Colours::black);

   g.drawImage(fretboard_img_, fretboard_loc_.getX(), fretboard_loc_.getY(),
               fretboard_loc_.getWidth(), fretboard_loc_.getHeight(), 0, 0,
               fretboard_img_.getWidth(), fretboard_img_.getHeight());

   // Iterate through each string
   for (unsigned int string{0}; string < fretboard_.GetTuning().size(); string++) {
      unsigned index = static_cast<unsigned>(fretboard_.GetTuning()[string].GetPitchName().ToInt());

      for (unsigned fret = 0; fret < frets; fret++) {
         if (fretboard_.GetPitchClassEnables()[index]) {
            std::stringstream temp_stream;
            temp_stream << fretboard_.GetPitchClassBuffer()[index];
            std::string temp;
            temp_stream >> temp;

            juce::Rectangle<int> dest_rect{fret_circle_locs_[string][fret].getX(),
                                           fret_circle_locs_[string][fret].getY(),
                                           fret_circle_locs_[string][fret].getWidth(),
                                           fret_circle_locs_[string][fret].getHeight()};
            juce::Rectangle<int> src_rect{0, 0, circle_img_.getWidth(), circle_img_.getHeight()

            };

            g.drawImage(circle_img_, dest_rect.getX(), dest_rect.getY(), dest_rect.getWidth(),
                        dest_rect.getHeight(), src_rect.getX(), src_rect.getY(),
                        src_rect.getWidth(), src_rect.getHeight());
            g.drawText(temp, fret_circle_locs_[string][fret], juce::Justification::centred, true);
         }

         index = (index + 1) % 12;
      }
   }

   // stringstream dance again .-. (I need to fix this in the lib)
   std::stringstream temp_stream;
   temp_stream << scale_.GetRoot().GetPitchName();
   std::string temp;
   temp_stream >> temp;

   std::string scale_name{temp + " " + scale_.GetName()};

   g.setColour(juce::Colours::white);
   g.drawText(scale_name, juce::Rectangle<int>{0, 580, 600, 80}, juce::Justification::topLeft);
}

void FretboardComponent::resized() {
   // This is called when the FretboardComponent is resized.
   // If you add any child components, this is where you should
   // update their positions.
}

void FretboardComponent::RandomizeScaleAndRoot() {
}

void FretboardComponent::RandomizeScale() {
}

void FretboardComponent::RandomizeRoot() {
}
